Push-Location $PSScriptRoot
Get-ChildItem -Path .\html | Remove-Item -Force
Get-ChildItem -Path .\nunit | Remove-Item -Force
Powershell.exe -Command invoke-pester ./ -OutputFormat NUnitXml -OutputFile "$PSScriptRoot/nunit/Windows.xml"
pwsh.exe -Command invoke-pester ./ -OutputFormat NUnitXml -OutputFile "$PSScriptRoot/nunit/WindowsCore.xml"
docker-compose.exe up
ii ./html/index.html